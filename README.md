# android-session-manager

This library implements functionality that facilitates the management of data that is usually
associated with common session management workflows. The data is organized into `Session`
objects which can then be persisted and retrieved by the `SessionManager`.
The `SessionManager` persists data using Android Shared Preferences and uses the
Android Content Resolver for communication to keep multiple instances of the
`SessionManager` in sync. This makes cross process use of the `SessionManger`
"just work", which is a notorious consideration when utilizing Android Services for
remote data management.

## How to install

This implementation is distributed as a single JAR archive which should be compiled into
your app's APK.

## How to use

The `SessionManager` class exposes an easy to use interface that allows the management
of `Session` objects.

## Sample code


    final SessionManager sessionManagerA = SessionManager.Factory.buildInstance(mContext, 1);

    final Session session = sessionManagerA.authenticate("MOBILE_CLIENT_APP", "johnappleseed", "mysecretkey", 5);

    session.setData("lastCheckedId", "32");
    session.setData("lastCheckedCount", "89");

    sessionManagerA.saveSession(session);


## License

LGPL-v3
