/*
 *     Copyright (C) 2016  Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.android.sessionmanager;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;


import java.util.HashMap;
import java.util.Map;

/**
 * Created by Aston Hamilton
 */
public final class SessionManager {
    private static final String FILE_NAME_SHARED_PREFERENCES_PREFIX = SessionManager.class.getName() + "_session_";
    private static final String PREF_KEY_AUTH_IDENTIFIER = "authIdentifier";
    private static final String PREF_KEY_AUTH_KEY = "authKey";

    private static final String PREF_KEY_DATA_SIZE = "dataSize";
    private static final String PREF_KEY_DATA_KEY_PREFIX = "dataKey_";
    private static final String PREF_KEY_DATA_VALUE_PREFIX = "dataValue_";

    private static final Uri URI_SESSION_MANAGER_ACTION = new Uri.Builder().scheme("content").authority(SessionManager.class.getName()).appendPath("session").appendPath("action").build();

    private final static String ACTION_AUTHENTICATE = "authenticate";
    private final static String ACTION_UNAUTHENTICATE = "unauthenticate";
    private final static String ACTION_REFRESH = "refresh";

    private final static String PARAM_ACTION = "action";
    private final static String PARAM_REALM = "realm";
    private final static String PARAM_AUTH_IDENTIFIER = "authIdentifier";
    private final static String PARAM_AUTH_KEY = "authKey";
    private final static String PARAM_DATA_CAPACITY = "authDatasize";


    private Context mContext;
    private final HashMap<String, Session> SESSION_MAP;

    private final ContentObserver OBSERVER_SESSION_MANAGER_ACTION = new ContentObserver((Handler) null) {
        @Override
        public void onChange(final boolean selfChange) {
            this.onChange(selfChange, null);
        }

        @Override
        public void onChange(final boolean selfChange, final Uri uri) {
            if (selfChange) {
                return;
            }

            final String action = uri.getQueryParameter(PARAM_ACTION);
            final String realm = uri.getQueryParameter(PARAM_REALM);
            final String authIdentifier = uri.getQueryParameter(PARAM_AUTH_IDENTIFIER);
            final String authKey = uri.getQueryParameter(PARAM_AUTH_KEY);
            int dataCapacity;

            Log.d("SessionManagerTestTag", "Change occurred: " + SessionManager.this.hashCode() + " " + action + " " + realm);

            try {
                final String dataCapacityParameter = uri.getQueryParameter(PARAM_DATA_CAPACITY);
                if (dataCapacityParameter != null) {
                    dataCapacity = Integer.parseInt(dataCapacityParameter);
                } else {
                    dataCapacity = 0;
                }
            } catch (final NumberFormatException e) {
                dataCapacity = 0;
            }


            if (ACTION_AUTHENTICATE.equals(action)) {
                authenticateImplementation(realm, authIdentifier, authKey, dataCapacity);
            } else if (ACTION_UNAUTHENTICATE.equals(action)) {
                unauthenticateImplementation(realm);
            } else if (ACTION_REFRESH.equals(action)) {
                final Session session = retrievePersistedSession(mContext, realm);
                if (session != null) {
                    saveSession(session);
                }
            }
        }
    };

    protected SessionManager(final Context context, final int realmCount, final boolean registerObserver) {
        mContext = context.getApplicationContext();

        SESSION_MAP = new HashMap<>(realmCount);

        if (registerObserver) {
            registerObserver();
        }
    }

    protected void registerObserver() {
        final ContentResolver contentResolver;

        contentResolver = mContext.getContentResolver();

        contentResolver.registerContentObserver(URI_SESSION_MANAGER_ACTION, true, OBSERVER_SESSION_MANAGER_ACTION);
    }

    public Session authenticate(final String realm, final String authIdentifier, final String authKey, final int dataCapacity) {
        mContext.getContentResolver().notifyChange(
                URI_SESSION_MANAGER_ACTION.buildUpon()
                        .appendQueryParameter(PARAM_ACTION, ACTION_AUTHENTICATE)
                        .appendQueryParameter(PARAM_REALM, realm)
                        .appendQueryParameter(PARAM_AUTH_IDENTIFIER, authIdentifier)
                        .appendQueryParameter(PARAM_AUTH_KEY, authKey)
                        .appendQueryParameter(PARAM_DATA_CAPACITY, dataCapacity + "")
                        .build()
                , OBSERVER_SESSION_MANAGER_ACTION);

        return authenticateImplementation(realm, authIdentifier, authKey, dataCapacity);
    }

    public void unauthenticate(final String realm) {
        mContext.getContentResolver().notifyChange(
                URI_SESSION_MANAGER_ACTION.buildUpon()
                        .appendQueryParameter(PARAM_ACTION, ACTION_UNAUTHENTICATE)
                        .appendQueryParameter(PARAM_REALM, realm)
                        .build()
                , OBSERVER_SESSION_MANAGER_ACTION);

        unauthenticateImplementation(realm);
    }

    public Session getSession(final String realm) {
        if (!SESSION_MAP.containsKey(realm)) {
            final Session session = retrievePersistedSession(mContext, realm);
            if (session == null) {
                return null;
            }

            SESSION_MAP.put(realm, session);
        }

        if (SESSION_MAP.get(realm).isDirty()) {
            final Session session = retrievePersistedSession(mContext, realm);
            if (session == null) {
                return null;
            }

            SESSION_MAP.put(realm, session);
        }

        return SESSION_MAP.get(realm);
    }

    public boolean isAuthenticated(final String realm) {
        return getSession(realm) != null;
    }

    protected void releaseObserver() {
        final ContentResolver contentResolver = mContext.getContentResolver();
        contentResolver.unregisterContentObserver(OBSERVER_SESSION_MANAGER_ACTION);
    }

    protected void releaseContext() {
        mContext = null;
    }

    public void saveSession(final Session session) {
        SessionManager.persistSession(mContext, session);


        mContext.getContentResolver().notifyChange(
                URI_SESSION_MANAGER_ACTION.buildUpon()
                        .appendQueryParameter(PARAM_ACTION, ACTION_REFRESH)
                        .appendQueryParameter(PARAM_REALM, session.getRealm())
                        .build()
                , OBSERVER_SESSION_MANAGER_ACTION);

        session.markChangesCommitted();

        SESSION_MAP.put(session.getRealm(), session);
    }

    private Session authenticateImplementation(final String realm, final String authIdentifier, final String authKey, final int dataCapacity) {
        final Session session = new Session(realm, authIdentifier, authKey, dataCapacity);

        SessionManager.persistSession(mContext, session);

        SESSION_MAP.put(session.getRealm(), session);

        return session;
    }

    private void unauthenticateImplementation(final String realm) {
        SessionManager.deleteSession(mContext, realm);

        SESSION_MAP.remove(realm);
    }

    private static Session retrievePersistedSession(final Context context, final String realm) {
        final SharedPreferences preferences = context.getSharedPreferences(FILE_NAME_SHARED_PREFERENCES_PREFIX + realm, Context.MODE_PRIVATE);
        final String authIdentifier, authKey;
        final int authDataSize;
        final Session session;

        authIdentifier = preferences.getString(PREF_KEY_AUTH_IDENTIFIER, null);
        authKey = preferences.getString(PREF_KEY_AUTH_KEY, null);

        if (authIdentifier == null || authKey == null) {
            return null;
        }

        authDataSize = preferences.getInt(PREF_KEY_DATA_SIZE, 0);

        session = new Session(realm, authIdentifier, authKey, authDataSize);
        for (int i = 0; i < authDataSize; i++) {
            final String authDataKey = preferences.getString(PREF_KEY_DATA_KEY_PREFIX + i, null);
            final String authDataValue = preferences.getString(PREF_KEY_DATA_VALUE_PREFIX + i, null);
            if (authDataKey == null) {
                continue;
            }

            session.setData(authDataKey, authDataValue);
        }

        return session;
    }

    @SuppressLint("CommitPrefEdits")
    private static void persistSession(final Context context, final Session session) {
        final Map<String, String> sessionData = session.getData();

        final SharedPreferences.Editor editor =
                context.getSharedPreferences(FILE_NAME_SHARED_PREFERENCES_PREFIX + session.getRealm(), Context.MODE_PRIVATE).edit()
                        .clear()
                        .putString(PREF_KEY_AUTH_IDENTIFIER, session.getAuthIdentifier())
                        .putString(PREF_KEY_AUTH_KEY, session.getAuthKey());


        editor.putInt(PREF_KEY_DATA_SIZE, sessionData.size());
        int i = 0;
        for (final Map.Entry<String, String> entry : sessionData.entrySet()) {
            editor.putString(PREF_KEY_DATA_KEY_PREFIX + i, entry.getKey());
            editor.putString(PREF_KEY_DATA_VALUE_PREFIX + i, entry.getValue());
            i++;
        }

        editor.commit();
    }

    private static void deleteSession(final Context context, final String realm) {
        context.getSharedPreferences(FILE_NAME_SHARED_PREFERENCES_PREFIX + realm, Context.MODE_PRIVATE)
                .edit().clear().commit();
    }

    public static class Factory {
        public static SessionManager buildInstance(final Context context, final int realmCount) {
            return new SessionManager(context, realmCount, true);
        }

        public static void disposeInstance(final SessionManager sessionManager) {
            sessionManager.releaseObserver();
            sessionManager.releaseContext();
        }
    }
}
