/*
 *     Copyright (C) 2016  Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.android.sessionmanager;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Aston Hamilton
 */
public class Session implements Parcelable {
    private final String mRealm;
    private final String mAuthIdentifier;
    private final String mAuthKey;
    private Map<String, String> mOriginalData;

    private boolean mDirty;
    private Map<String, String> mData;


    private Session(final String realm, final String authIdentifier, final String authKey) {
        this(realm, authIdentifier, authKey, 10);
    }

    public Session(final String realm, final String authIdentifier, final String authKey, final int sessionDataCapacity) {
        this(realm, authIdentifier, authKey, new HashMap<String, String>(sessionDataCapacity));
    }

    protected Session(Parcel in) {
        //noinspection unchecked
        this(in.readString(), in.readString(), in.readString(), in.readHashMap(String.class.getClassLoader()));

        mDirty = in.readInt() == 1;
        //noinspection unchecked
        mData = in.readHashMap(String.class.getClassLoader());
    }

    protected Session(final String realm, final String authIdentifier, final String authKey, final Map<String, String> data) {
        mRealm = realm;
        mAuthIdentifier = authIdentifier;
        mAuthKey = authKey;
        mData = mOriginalData = data;

        mDirty = false;
    }

    public static final Creator<Session> CREATOR = new Creator<Session>() {
        @Override
        public Session createFromParcel(Parcel in) {
            return new Session(in);
        }

        @Override
        public Session[] newArray(int size) {
            return new Session[size];
        }
    };

    public String getRealm() {
        return mRealm;
    }

    public String getAuthIdentifier() {
        return mAuthIdentifier;
    }

    public String getAuthKey() {
        return mAuthKey;
    }

    public Map<String, String> getData() {
        return Collections.unmodifiableMap(mData);
    }

    public String getData(final String key) {
        return mData.get(key);
    }

    public void setData(final String key, final String value) {
        if (!mDirty) {
            mDirty = true;

            mData = new HashMap<>(mOriginalData);
        }

        mData.put(key, value);
    }

    public boolean isDirty() {
        return mDirty;
    }

    public void discardChanges() {
        mData = mOriginalData;
        mDirty = false;
    }

    public void markChangesCommitted() {
        mOriginalData = mData;
        mDirty = false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeString(mRealm);
        dest.writeString(mAuthIdentifier);
        dest.writeString(mAuthKey);
        dest.writeMap(mOriginalData);
        dest.writeInt(mDirty ? 1 : 0);
        dest.writeMap(mData);
    }

    @Override
    public boolean equals(final Object o) {
        if (o instanceof Session) {
            return (mRealm == null ? ((Session) o).getRealm() == null : mRealm.equals(((Session) o).getRealm()))
                    && (mAuthIdentifier == null ? ((Session) o).getAuthIdentifier() == null : mAuthIdentifier.equals(((Session) o).getAuthIdentifier()))
                    && (mAuthKey == null ? ((Session) o).getAuthKey() == null : mAuthKey.equals(((Session) o).getAuthKey()))
                    && (mData == null ? ((Session) o).getData() == null : mData.entrySet().equals(((Session) o).getData().entrySet()));
        }
        return super.equals(o);
    }
}
